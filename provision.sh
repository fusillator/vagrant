#!/bin/bash
yum -y install nginx bind-utils lsof
yum -y install policycoreutils-python setools-console setroubleshoot-server
yum -y update
mv /usr/share/nginx/html /usr/share/nginx/html.default
ln -s /data/web/html /usr/share/nginx/html
cp /etc/nginx/nginx.conf /etc/nginx/nginx.conf.default
cp /data/web/nginx.conf /etc/nginx/nginx.conf 
[ -f /data/web/server.crt -a -f /data/web/server.key ] || openssl req -newkey rsa:2048 -nodes -keyout /data/web/server.key -x509 -days 365 -out /data/web/server.crt -subj "/C=IT/ST=Milan/L=Milan/O=testina/CN=webhat/emailAddress=fusillator@gmail.com"
mkdir -p /etc/pki/nginx/private
cp /data/web/server.crt /etc/pki/nginx/server.crt
cp /data/web/server.key /etc/pki/nginx/private/server.key
semanage fcontext --add --type bin_t /usr/sbin/nginx
restorecon -v /usr/sbin/nginx
usermod -a -G vboxsf vagrant
systemctl enable nginx
systemctl start nginx
