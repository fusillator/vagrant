# README #

### What is this repository for? ###

My deployment tries with vagrant.
 
### How do I get set up? ###

* Configuration  
I use the vbox available on https://app.vagrantup.com/centos/boxes/7 as a base, see the deployment section for further details to enable the missing guest additions.
My test enviroment is a single host geared with xubuntu os and oracle virtual box. 
  
* Deployment instructions  
The centos vbox image available on vagrant repo doesn't include VBoxGuestAdditions, so these are the steps to forge a new box with the missing stuff. 

```
fusillator@catorcio:~/vbox/centos$ vagrant init centos/7 
fusillator@catorcio:~/vbox/centos$ vagrant up
fusillator@catorcio:~/vbox/centos$ vagrant ssh
```
the image VBoxGuestAdditions.iso is usually included by virtualbox package or available for download on http://download.virtualbox.org/virtualbox/5.x.x/VBoxGuestAdditions_5.x.x.iso
The easiest way to share the iso on the guest is by the virtualbox menu "Insert Guest Additions CD image..." avaialable on the guest windows 
```
[vagrant@webhat ~]$ sudo yum -y install epel-release wget kernel-devel kernel-headers gcc gcc-c++ make
[vagrant@webhat ~]$ wget -q -O- https://raw.githubusercontent.com/mitchellh/vagrant/master/keys/vagrant.pub > .ssh/authorized_keys
[vagrant@webhat ~]$ sudo mount /dev/cdrom /mnt -r
[vagrant@webhat ~]$ sudo /mnt/VBoxLinuxAdditions.run
Verifying archive integrity... All good.
Uncompressing VirtualBox 5.1.26 Guest Additions for Linux...........
VirtualBox Guest Additions installer
Copying additional installer modules ...
Installing additional modules ...
vboxadd.sh: Starting the VirtualBox Guest Additions.
```
replace the name centos_default_1504363542053_19679 with the actual name of the vm in the following snippet
```
fusillator@catorcio:~/vbox/centos$ vagrant halt
fusillator@catorcio:~/vbox/centos$ vagrant package --base centos_default_1504363542053_19679
==> centos_default_1504363542053_19679: Clearing any previously set forwarded ports...
==> centos_default_1504363542053_19679: Exporting VM...
==> centos_default_1504363542053_19679: Compressing package to: /home/fusillator/vbox/centos/package.box
fusillator@catorcio:~/vbox/centos$ vagrant box add centos_vbguestaddition package.box 
==> box: Box file was not detected as metadata. Adding it directly...
==> box: Adding box 'centos_vbguestaddition' (v0) for provider: 
    box: Unpacking necessary files from: file:///home/fusillator/vbox/centos/package.box
==> box: Successfully added box 'centos_vbguestaddition' (v0) for 'virtualbox'!
```

### Contribution guidelines ###

* Code review  
open a git pull request for any issue  

### Who do I talk to? ###

mailto fusillator@gmail.com   
